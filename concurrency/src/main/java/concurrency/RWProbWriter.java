package concurrency;

import org.apache.log4j.Logger;


public class RWProbWriter extends RWProblemThread implements ConcurrentWriter{
	private static final Logger logger = Logger.getLogger(RWProbWriter.class.getName());
	public RWProbWriter(Controller controller, String name) {
		super(controller, name);
	}

	@Override
	public boolean isReader() {
		return false;
	}

	@Override
	public boolean isWriter() {
		return true;
	}

	public void write(SharedObject obj) {
		obj.increment();
		log("I am writing :: "+obj.getVal());
	}
	@Override
	public Logger getLogger() {
		return logger;
	}
}
