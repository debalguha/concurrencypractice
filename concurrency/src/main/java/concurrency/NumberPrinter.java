package concurrency;

import java.util.concurrent.Semaphore;
import java.util.logging.Logger;

public class NumberPrinter implements Runnable{
	private static final Logger logger = Logger.getLogger(NumberPrinter.class.getName());
	private int seed;
	private Semaphore semA;
	private Semaphore semB;
	private String threadName;
	private int limit;
	public NumberPrinter(int seed, int limit, Semaphore semA, Semaphore semB, String threadName){
		this.seed = seed;
		this.limit = limit;
		this.semA = semA;
		this.semB = semB;
		this.threadName = threadName;
	}
	public void run() {
		while(seed<=limit){
			try {
				semA.acquire();
				logger.info(threadName+" : "+seed);
				seed+=2;
				semB.release();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
