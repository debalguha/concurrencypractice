package concurrency;

public interface Controller {
	public void doControl(RWProblemThread thread);
}
