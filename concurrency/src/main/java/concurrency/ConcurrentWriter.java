package concurrency;

public interface ConcurrentWriter {
	public void write(SharedObject obj);
}
