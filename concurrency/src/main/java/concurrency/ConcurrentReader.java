package concurrency;

public interface ConcurrentReader {
	public void read(SharedObject obj);
}
