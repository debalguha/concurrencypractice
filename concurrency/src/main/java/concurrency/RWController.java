package concurrency;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class RWController implements Controller{
	private ReentrantReadWriteLock rwLock;
	private AtomicInteger readerCounter;
	private AtomicInteger writerCounter;
	private AtomicInteger waitingWriterCounter;
	SharedObject sharedResource;
	public RWController(){
		readerCounter = new AtomicInteger(0);
		writerCounter = new AtomicInteger(0);
		waitingWriterCounter = new AtomicInteger(0);
		rwLock = new ReentrantReadWriteLock(true);
		sharedResource = new SharedObject(0);
	}
	
	public static void main(String args[]){
		
		RWController controller = new RWController();
		RWProblemThread reader1 = new RWProbReader(controller, "Reader1");
		RWProblemThread reader2 = new RWProbReader(controller, "Reader2");
		
		RWProblemThread writer1 = new RWProbWriter(controller, "Writer1");
		RWProblemThread writer2 = new RWProbWriter(controller, "Writer2");
		
		Executor executor = Executors.newFixedThreadPool(10);
		executor.execute(reader1);
		executor.execute(writer1);
		executor.execute(reader2);
		executor.execute(writer2);
		
	}
	
	public void doControl(RWProblemThread thread){
		if(thread.isReader()){
			this.read(((ConcurrentReader)thread));
			this.doneReading((ConcurrentReader)thread);
		}else{
			this.write((ConcurrentWriter)thread);
			this.doneWriting((ConcurrentWriter)thread);
		}
	}
	private void read(ConcurrentReader reader){
		while(true){
			rwLock.readLock().lock();
			((RWProblemThread)reader).log("Locking rwLock");
			if(writerCounter.compareAndSet(0, 0) && waitingWriterCounter.compareAndSet(0, 0)){
				readerCounter.incrementAndGet();
				((RWProblemThread)reader).log("No writer unlocking.");
				rwLock.readLock().unlock();
				reader.read(sharedResource);
				return;
			}
			((RWProblemThread)reader).log("Unlocking as Writer exists. Going to wait --> active writers:: "+writerCounter.intValue()+", active waiting writers :: "+waitingWriterCounter.intValue());
			rwLock.readLock().unlock();
			//synchronized(mutexWriter){
			synchronized(this){
				try {
					((RWProblemThread)reader).log("waiting");
					wait();
				} catch (InterruptedException e) {}
			}
		}
	}
	private void write(ConcurrentWriter writer){
		while(true){
			rwLock.writeLock().lock();
			((RWProblemThread)writer).log("Locking rwLock");
			waitingWriterCounter.incrementAndGet();
			if(readerCounter.compareAndSet(0, 0)){
				waitingWriterCounter.decrementAndGet();
				writerCounter.incrementAndGet();
				((RWProblemThread)writer).log("No reader, written and unlocking.");
				rwLock.writeLock().unlock();
				writer.write(sharedResource);
				return;
			}
			((RWProblemThread)writer).log("Unlocking as reader exists. Going to wait --> readers active:: "+readerCounter.intValue());
			rwLock.writeLock().unlock();
			//synchronized (mutexReader) {
			synchronized (this) {
				try {
					((RWProblemThread)writer).log("waiting");
					wait();
				} catch (InterruptedException e) {}
			}
		}
	}


	private void doneReading(ConcurrentReader reader) {
		readerCounter.decrementAndGet();
		try {
			notifyAll();
			((RWProblemThread)reader).log("Writer notified.");
		} catch (Exception e) {
			((RWProblemThread)reader).log("Exception "+e.getMessage());
			e.printStackTrace();
		}
	}
	private void doneWriting(ConcurrentWriter writer){
		try {
			writerCounter.decrementAndGet();
			notifyAll();
			((RWProblemThread)writer).log("Reader notified.");
		} catch (Exception e) {
			((RWProblemThread)writer).log("Exception "+e.getMessage());
			e.printStackTrace();
		}
	}
}
