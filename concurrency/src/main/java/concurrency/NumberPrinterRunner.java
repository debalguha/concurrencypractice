package concurrency;

import java.util.concurrent.Semaphore;
import java.util.logging.Logger;

public class NumberPrinterRunner {
	private static final Logger logger = Logger.getLogger(NumberPrinterRunner.class.getName());
	public static void main(String[] args) throws InterruptedException {
		Semaphore semA = new Semaphore(1);
		Semaphore semB = new Semaphore(1);
		NumberPrinter oddPrinter = new NumberPrinter(1, 10000, semA, semB, "Thread A");
		NumberPrinter evenPrinter = new NumberPrinter(2, 10000, semB, semA, "Thread B");
		semB.acquire();
		logger.info("Starting threads");
		new Thread(evenPrinter).start();
		new Thread(oddPrinter).start();
	}

}
