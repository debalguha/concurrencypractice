package concurrency;

public class SharedObject {
	private int val;
	public SharedObject(int val) {
		super();
		this.val = val;
	}
	public void increment(){
		this.val++;
	}
	public int getVal(){
		return val;
	}
}
