package concurrency;

import org.apache.log4j.Logger;


public abstract class RWProblemThread extends Thread{
	protected Controller controller;
	protected String name;
	public RWProblemThread(Controller controller, String name){
		this.controller = controller;
		this.name = name;
	}
	public void run() {
		controller.doControl(this);
	}
	
	public abstract boolean isReader();
	public abstract boolean isWriter();
	public abstract Logger getLogger();
	
	public void log(String msg){
		getLogger().info(name+"-->"+msg);
	}
}
