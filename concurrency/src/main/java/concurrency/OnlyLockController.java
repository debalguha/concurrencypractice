package concurrency;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class OnlyLockController implements Controller {
	private ReentrantReadWriteLock rwLock;
	private AtomicInteger readerCounter;
	private AtomicInteger writerCounter;
	private AtomicInteger waitingWriterCounter;
	private Object mutex;
	private SharedObject sharedResource;

	public OnlyLockController() {
		readerCounter = new AtomicInteger(0);
		writerCounter = new AtomicInteger(0);
		waitingWriterCounter = new AtomicInteger(0);
		rwLock = new ReentrantReadWriteLock(true);
		mutex = new Object();
		sharedResource = new SharedObject(0);
	}

	public static void main(String args[]) {
		OnlyLockController controller = new OnlyLockController();
		RWProblemThread reader1 = new RWProbReader(controller, "Reader1");
		RWProblemThread reader2 = new RWProbReader(controller, "Reader2");
		RWProblemThread reader3 = new RWProbReader(controller, "Reader3");
		RWProblemThread reader4 = new RWProbReader(controller, "Reader4");
		RWProblemThread reader5 = new RWProbReader(controller, "Reader5");

		RWProblemThread writer1 = new RWProbWriter(controller, "Writer1");
		RWProblemThread writer2 = new RWProbWriter(controller, "Writer2");

		Executor executor = Executors.newFixedThreadPool(10);
		executor.execute(reader1);
		executor.execute(writer1);
		executor.execute(reader2);
		executor.execute(reader3);
		executor.execute(reader4);
		executor.execute(reader5);
		executor.execute(writer2);

	}

	public void doControl(RWProblemThread thread) {
		if (thread.isReader()) {
			this.read(((ConcurrentReader) thread));
		} else {
			this.write((ConcurrentWriter) thread);
		}
	}

	private void write(ConcurrentWriter writer) {
		try {
			Thread.sleep(3);
		} catch (InterruptedException e) {}
		waitingWriterCounter.incrementAndGet();
		((RWProblemThread)writer).log("I have registered");
		rwLock.writeLock().lock();
		/*try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {}*/
		((RWProblemThread)writer).log("Obtained Lock");
		synchronized(mutex){
			writerCounter.incrementAndGet();
			writer.write(sharedResource);
			writerCounter.decrementAndGet();
			waitingWriterCounter.decrementAndGet();
			((RWProblemThread)writer).log("Releasing Lock");
			rwLock.writeLock().unlock();
			mutex.notifyAll();
		}
	}

	private void read(ConcurrentReader reader) {
		try {
			Thread.sleep(2);
		} catch (InterruptedException e) {}
		while (true) {
			rwLock.readLock().lock();
			((RWProblemThread)reader).log("Obtained Lock");
			if (canRead()) {
				readerCounter.incrementAndGet();
				reader.read(sharedResource);
				readerCounter.decrementAndGet();
				((RWProblemThread)reader).log("Releasing Lock");
				rwLock.readLock().unlock();
				return;
			}
			rwLock.readLock().unlock();
			synchronized(mutex){
				try {
					((RWProblemThread)reader).log("I am waiting");
					mutex.wait();
				} catch (InterruptedException e) {
				}
			}
		}

	}

	private synchronized boolean canRead() {
		return writerCounter.compareAndSet(0, 0) && waitingWriterCounter.compareAndSet(0, 0);
	}
	
}
