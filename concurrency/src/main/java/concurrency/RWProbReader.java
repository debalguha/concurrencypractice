package concurrency;

import org.apache.log4j.Logger;


public class RWProbReader extends RWProblemThread implements ConcurrentReader {
	
	private static final Logger logger = Logger.getLogger(RWProbReader.class.getName());
	
	public RWProbReader(Controller controller, String name) {
		super(controller, name);
	}
	@Override
	public boolean isReader() {
		return true;
	}
	@Override
	public boolean isWriter() {
		return false;
	}
	public void read(SharedObject obj) {
		log("I am reading :: "+obj.getVal());
	}
	@Override
	public Logger getLogger() {
		return logger;
	}

}
